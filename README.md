# Sonification Project in SuperCollider

## Overview

This project aims to translate temperature data into sound, focusing on the period from January to September in the years 2003 and 2023. The data comes from three of the most northern universities: Tromsø, Norway; Fairbanks, Alaska, USA; and Oulu, Finland. The goal is to sonically represent the impact of global warming, with audio changes reflecting variations in temperature.

### Data Source

- **Tromsø, Norway**
  - Latitude: 69.679
  - Longitude: 18.971
- **Fairbanks, Alaska, USA**
  - Latitude: 64.857
  - Longitude: -147.823
- **Oulu, Finland**
  - Latitude: 65.059
  - Longitude: 25.466

The CSVFileReader is used to read data from a CSV file. The data is organized in rows, each containing temperature data for a specific month's average from January until September in 2003 and 2023, showing the differences due to climate change.

### Synthesis Process

Using SuperCollider, the temperature data controls the amplitude (volume) of three sine wave oscillators at different frequencies:

- **Tromsø:** 2000 Hz
- **Fairbanks:** 1000 Hz
- **Oulu:** 1500 Hz

Panning is used to represent the extremity of the temperature difference.

### Code Snippet

```supercollider
(
    y = CSVFileReader.read("/home/lydia/Downloads/sonification2.csv", true).postcs;

    ~player = SynthDef('player', 
        { |amp = 1, amp2 = 1, amp3 = 1|
            var sin = SinOsc.ar(2000, 0, amp);   // Tromsø
            var sin2 = SinOsc.ar(1000, 0, amp2); // Fairbanks
            var sin3 = SinOsc.ar(1500, 0, amp3); // Oulu
            Out.ar([0, 1], Sum3(sin, sin2, sin3))
        }
    ).play;

    i = 1;
    fork {
        y.size.do {
            var t_amount, f_amount, o_amount;
            t_amount = y[i][0].postln;
            f_amount = y[i][2].postln;
            o_amount = y[i][4].postln;
            ~player.set(\amp, t_amount.asFloat);
            ~player.set(\amp2, f_amount);
            ~player.set(\amp3, o_amount);
            i = i + 1;
            1.wait;
        }
    }.play;
)

//2 
(
fork {
        var ssss = 666;
    
ssss.do
 { |i|
        var ev, startWs, endWs;
        startWs = i * 20; endWs = 1 + 200;
       ev = w.eventFor(startWs,  endWs, 1, exprand(0.5, 1.0));
        ev.put(\pan, 1.0.rand2).play;
        ev.sustain.wait;
        // ev.set(\startWs, 1200);
        i.value.postln;
    }
};
)

Quarks.gui

// 2

(
    ~path = (PathName(thisProcess.nowExecutingPath)).pathOnly;
    w = Wavesets.from(~path ++ "/excitation.wav");
)

(
    Wavesets.prepareSynthDefs;
    fork {
        var times = 666;
        var base = "aab";
        var rule = ["aa" -> "ab", "a" -> "b", "bb" -> "ab", "b" ->"ba"];
        var note = 60;
        
times.do
 { |i, startWs, endWs, repeat|
        var ev;
        startWs = i * 20; endWs = 1 + 200; repeat = 1;
        ev = w.eventFor(startWs,  endWs, 1, exprand(0.5, 1.0));
        ev.put(\pan, 1.0.rand2);
        ev.put(\lsys, Plsys(Pseq(base), rule, 64)).play;
        ev.set(\startWs, Pfunc({|ev| if(ev.lsys.asString == "a", { startWs = 3}, { startWs = exprand(i * 2, i + 20000).round(1)}); startWs }));
        ev.set(\endWs, Pfunc({|ev| if(ev.lsys.asString == "ab", {200}, {4000})}));
        ev.set(\repeat, Pfunc({|ev| if(ev.lsys.asString == "a", {20}, {10})}));
        ev.sustain.wait;
    }
    };
)

// 3


(
fork {
        var ssss = 666;
    
ssss.do
 { |i|
        var ev, startWs, endWs;
        startWs = i * 20; endWs = 1 + 200;
       ev = w.eventFor(startWs,  endWs, 1, exprand(0.5, 1.0));
        ev.put(\pan, 1.0.rand2).play;
        ev.sustain.wait;
        // ev.set(\startWs, 1200);
        i.value.postln;
    }
};
)

// 4. Microsound Etude

//I managed to reconstruct the problem #4. In the original example on my other laptop I also had another Ndef that was influencing this one which was just an envelope with silences and a Pdef (using NodeProxy roles). It was hard enough to figure it out the first time, so I am not able to reproduce it in time here. 

(
    Ndef(\lc, { |freqs = 800, pan = 0| 
        var sig = LFTri.ar(freqs);
        sig = Pan2.ar(sig, pan);
        sig * 0.1;
    }).play;
)

Ndef(\lc).fadeTime = 4;

Ndef(\lc).xset(\freqs, rrand(80, 500), \pan, rrand(-1.0, 1.0));

// Phase two
(
    Ndef(\lc)[1] = \filter->{
        |in, shiftFreq = 1| FreqShift.ar(in, shiftFreq) 
    }
);

Ndef(\lc).xset(\shiftFreq, 200, \wet1, 0.5);

Ndef(\lc).xset(\freqs, rrand(80, 500), \pan, rrand(-1.0, 1.0));
Ndef(\lc).xset(\shiftFreq, rrand(100, 300), \wet1, rrand(0.5, 0.9), \pan, rrand(-0.5, 0.5));



// Phase 3
(
    Ndef(\lc)[2] = \filter-> {
        | in, pitchShift = 2, pitchDispersion = 0.1, timeDispersion = 0.1| PitchShift.ar(in, 0.25, pitchShift, pitchDispersion, timeDispersion)
    }
)



Ndef(\lc).xset(\timeDispersion, 1, \pitchDispersion, 0.25, \pitchShift, 10)


Ndef(\lc).xset(\freqs, rrand(600, 2000), \pan, rrand(-1.0, 1.0));
Ndef(\lc).xset(\shiftFreq, rrand(400, 1000), \wet1, rrand(0.5, 0.9));
Ndef(\lc).xset(\timeDispersion, rrand(0.8, 10), \pitchDispersion, rrand(0.5, 20), \pitchShift, rrand(7, 200));


(
  Ndef(\lc)[3] = \xset -> Pbind(
    \dur, Prand([1, 2, 5, 6, 0.2, 0.05, 1.5, 0.002], inf),
    \freqs, Pwhite(200, 1000, inf),
    \pitchDispersion, Prand([1, 0.2, 5, 10], inf),
    \timeDispersion, Prand([0.2, 0.3, 4, 10, 0.002], inf),
    \shiftFreq, Pseq([20, 100, 300, 2, 4, 39, 35], inf)
    )
)


(
  Ndef(\lc)[3] = \pset -> Pbind(
    \dur, Prand([1, 2, 5, 6, 0.2, 1.5 ], inf),
    \freqs, Pwhite(200, 1000, inf),
    \pitchDispersion, Prand([1, 0.2, 5, 10], inf),
    \timeDispersion, Prand([0.2, 0.3, 4, 10, 0.002], inf),
    \shiftFreq, Pseq([20, 100, 300, 2, 4, 39, 35], inf)
    )
)


(
    Ndef(\lc)[4] = \filter-> { 
        | in, mix = 0.33, room = 0.2|
        FreeVerb.ar(in, mix, room) 
    }
)


Ndef(\lc).gui

